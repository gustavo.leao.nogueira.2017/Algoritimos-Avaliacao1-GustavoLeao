/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Blog.es.esy.gustavoti;

import java.util.Scanner;

/**
 *
 * @author Gustavo Leão Nogueira de Oliveira
 * @version 1.0
 */
public class Estatistica {
    static final int tamanho = 20;
    static String vNome[] = new String[tamanho];
    static int vIdade[] = new int[tamanho];
    static char vSexo[] = new char[tamanho];
    
    public static void main(String[] args) {
        menu();
    }

    private static void menu() {
        String menuString[] = new String[]{
            "1 - Cadastrar dados", 
            "2 - Localizar Pessoas",
            "3 - Diferença de idades",
            "4 - Sair"
        };
        switch(lerMenu(menuString)){
            case 1: cadastro(); break;
            case 2: localizacao(); break;
            case 3: diferenca(); break;
            case 4: System.out.println("Então até mais!"); break;
        }
    }

    private static int lerMenu(String[] menuString) {
        System.out.println("Menu:");
        for (String menuString1 : menuString) {
            System.out.println("\t\t" + menuString1);
        }
        System.out.println("\t\t"+"Escreva:");
        int opcao = new Scanner(System.in).nextInt();
        return opcao;
    }

    private static void diferenca() {
        
    }

    private static void localizacao() {
        boolean continuar = true;
        int i = 0;
        System.out.println("Digite o nome da pessoa: ");
        String nome = new Scanner(System.in).nextLine();
        do{
            if(vNome[i].contains(nome)){ 
                continuar = false; 
                System.out.println("O nome ["+vNome[i]+"] foi achado!");
                System.out.println("E os dados associados a esse nome:");
                System.out.println(
                        "Nome:  \t"+vNome[i]+"\t"+
                        "Idade: \t"+vIdade[i]+"\t"+
                        "Sexo:  \t"+vSexo[i]+"\t");
            }
            else { 
                continuar = true;
                i++; 
            }
        }while(continuar == true);
    }

    private static void cadastro() {
        for(int i = 0; i < tamanho; i++){
            System.out.println("");
            System.out.println("Usuário :"+i);
            System.out.println("Digite a nome do "+i+" usuario:");
            vNome[i] = new Scanner(System.in).nextLine();
            System.out.println("Digite a idade do "+i+" usuario:");
            vIdade[i] = new Scanner(System.in).nextInt();
            System.out.println("Digite a sexo do "+i+" usuario: [Masculino - Feminino] ");
            vSexo[i] = new Scanner(System.in).nextLine().charAt(0);
            System.out.println("");
        }
    }
    
    
}
